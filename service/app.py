import os
import sys
import uuid

from flask import Flask
from flask import render_template
from flask import request
from flask import abort
from flask import flash
from flask import redirect
from werkzeug.utils import secure_filename
from flask_table import Table, Col
from os.path import join, dirname, realpath

from database import db
from database import init_db

from ml_service import ml_handler
from ml_service import load_model


init_db()


UPLOAD_FOLDER = join(dirname(realpath(__file__)), 'images')
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])


app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

uid = None

load_model()


def check_valid_ip(ip):
    if ip.split('.')[0] in [i.split('.')[0] for i in db.credentials['ip']]:
        return True
    else:
        return False


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# ---------------- ENDPOINTS -----------------------


@app.route("/", methods=['GET'])
def index():
    """
    Возвращает таблицу с статистикой по ip.
    """
    if check_valid_ip(ip=request.remote_addr):
        class ItemTable(Table):
            uuid = Col('uuid')
            description = Col('Description')
            _object = Col('Object')
            probability = Col('Probability')

        items = []

        try:
            for i in db.results.find({'ip': request.remote_addr}):
                items.append(
                    dict(
                        uuid=i['uid'],
                        description=i['description'],
                        _object=i.get('_object'),
                        probability=i.get('probability'),
                    )
                )
        except TypeError:
            items = []
        except AttributeError:
            items = []
        
        table = ItemTable(items)
        table.border = True

        return render_template('index.html', table=table)
    else:
        abort(404)


@app.route("/upload", methods=['GET'])
def upload():
    return render_template('upload.html')


@app.route("/start_searching", methods=['POST'])
def start_searching():
    description = request.form.get('description')

    if 'photo' not in request.files:
        return 'Ты втираешь мне какую-то дичь!'

    file = request.files['photo']

    if file.filename == '':
        return 'Ты втираешь мне какую-то дичь!'

    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(
            os.path.join(app.config['UPLOAD_FOLDER'], filename)
        )

        ml_result = ml_handler(
            img_path=os.path.join(app.config['UPLOAD_FOLDER'], filename),
        )

        uid = uuid.uuid4()

        db.results.insert_one({
            'ip': request.remote_addr,
            'uid': uid,
            '_object': ml_result['_object'],
            'probability': ml_result['probability'],
            'description': description,
        })

        if ml_result is not None:
            return redirect('/')
        else:
            return redirect('/check_result')


@app.route("/check_result", methods=['GET'])
def check_result():
    if db.results[request.remote_addr].get(uid) is not None:
        return redirect('/')
    else:
        return render_template('in_progress.html')


if __name__ == "__main__":
    app.secret_key = 'super secret key'
    app.run(host="0.0.0.0", debug=True)

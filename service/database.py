from pymongo import MongoClient


db_client = MongoClient(
    host="mongodb://root:root@database_service:27017",
)
db = db_client.ml_trial


def init_db():
    db.credentials = {
        'ip': [
            '172.*.*.*',
            '192.*.*.*',
        ]
    }

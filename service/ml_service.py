from keras.applications.vgg19 import VGG19
from keras.preprocessing import image
from keras.applications.vgg19 import preprocess_input, decode_predictions

import tensorflow as tf
import numpy as np
import sys


def load_model():
    # Создаем модель с архитектурой VGG19 и загружаем веса, обученные
    # на наборе данных ImageNet
    global model
    model = VGG19(weights='imagenet')

    global graph
    graph = tf.get_default_graph()


def ml_handler(img_path):

    # Загружаем изображение для распознавания, преобразовываем его в массив
    # numpy и выполняем предварительную обработку
    with graph.as_default():
        img = image.load_img(img_path, target_size=(224, 224))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)

        # Запускаем распознавание объекта на изображении
        preds = model.predict(x)

        # Отдаем первый класс объекта с самой высокой вероятностью
        # print('Результаты распознавания:', decode_predictions(preds, top=1)[0][0][1], file=sys.stderr)

        data = decode_predictions(preds, top=1)[0][0]

        return {
            '_object': data[1],
            'probability': str(round(data[2] * 100, 1)),
        }

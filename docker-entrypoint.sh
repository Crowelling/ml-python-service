#!/bin/bash

wait_for_port_available() {
    # $1 - service name
    # $2 - port

    local ATTEMPTS=10
    local TIMEOUT=5s

    while ! nc -z $1 $2; do
        if [[ $ATTEMPTS < 0 ]]; then
            echo "$1 is unavailable"
            exit 2
        fi

        echo "wait for $1..."

        sleep $TIMEOUT
        let ATTEMPTS--
    done
}

wait_for_database_server() {
    wait_for_port_available $DATABASE_SERVER $DATABASE_SERVER_PORT
}

case $1 in
start)
    wait_for_database_server
    exec python3 service/app.py
    ;;
*)
    exec $1 ${@:2}
    ;;
esac

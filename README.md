This service demostrates how to combine Flask, mongoDB, Keras+TensorFlow, Docker

## Tools

+ Python 3.7.3
+ see ./requirements.txt


## Must have

+ docker + docker-compose: 
https://hub.docker.com/


## How to get up this on your local machine?

1. get source into your local machine;
2. install docker + docker-compose;
3. up ram memory for docker daemon: Docker Desctop -> Preferences -> Advanced -> up to 6 gb;
4. install requirements.txt to your virt environment (if you need to edit and run separatly);
5. run by 'docker-compose up --build' from the head directory of product.

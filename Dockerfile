FROM python:3.7.3
ENV DATABASE_SERVER=db \
    DATABASE_SERVER_PORT=27017
EXPOSE 5000
WORKDIR /
COPY . /
RUN apt-get update && apt-get install -q -y netcat
RUN pip3 install -r requirements.txt
ENTRYPOINT ["bash", "docker-entrypoint.sh"]
CMD ["start"]